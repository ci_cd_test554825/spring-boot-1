FROM eclipse-temurin:17-jre-jammy
VOLUME /tmp
COPY target/backend.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]


